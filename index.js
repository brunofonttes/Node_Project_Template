const express = require('express');
const app = express();
const http = require('http').Server(app);
const consign = require('consign');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

global.getmodule = function getmodule(modulePath) {
    var path = require('path');
    return require(path.resolve(modulePath));
}

consign()
    .include('controllers')
    .then('routes')
    .into(app);

var port = process.env.PORT || 3006;

http.listen(port, function () {
    console.log('Servidor rodando em http://localhost:%s', port);
})

